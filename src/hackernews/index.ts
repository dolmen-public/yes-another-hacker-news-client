export * as HackerNewsModels from './models'
export * as HackerNewsRepositories from './repositories'
export * as HackerNewsUseCases from './useCases'
export * as HackerNewsComponent from './component'

import { UseCases } from './useCases'
import { Repositories } from './repositories'

export type HackerNewsServices = Readonly<UseCases> & Readonly<Repositories>
