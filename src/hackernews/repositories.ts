import { Item, ItemId, SortedStory } from './models'

type Unsubscribe = () => void

export interface StoryRepository {
  getBests(): Promise<ItemId[]>

  onBestsChanges(onchange: (ids: SortedStory[]) => Promise<void>): Unsubscribe

  getTops(): Promise<ItemId[]>

  onTopsChanges(onchange: (ids: SortedStory[]) => Promise<void>): Unsubscribe
}

export interface ItemRepository {
  getItem(id: ItemId): Promise<Item>

  onChange(id: ItemId, onchange: (item: Item) => Promise<void>): Unsubscribe
}

export interface PersonnalRepository {
  getSkippedStories(): Promise<Set<ItemId>>

  syncAllSkippedStories(skippedStories: Set<ItemId>): Promise<void>

  addSkippedStory(story: ItemId): Promise<void>
  onSkippedStoryAdded(onchange: (story: ItemId) => Promise<void>): Unsubscribe

  removeSkippedStory(story: ItemId): Promise<void>
  onSkippedStoryRemoved(onchange: (story: ItemId) => Promise<void>): Unsubscribe
}

export interface Repositories {
  StoryRepository: StoryRepository
  PersonnalRepository: PersonnalRepository
  ItemRepository: ItemRepository
}
