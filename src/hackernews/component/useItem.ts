import { Item, ItemId } from '../models'
import { Repositories } from '../repositories'
import React from 'react'

export type useItemProps = {
  repositories: Repositories
  id: ItemId
  hotReload?: boolean
}

export function useItem({ repositories, id, hotReload }: useItemProps) {
  const [item, setItem] = React.useState<Item | null>(null)

  React.useEffect(() => {
    repositories.ItemRepository.getItem(id).then((story) => setItem(story))

    const unsubcribe = hotReload ? repositories.ItemRepository.onChange(id, async (story) => setItem(story)) : () => {}
    return () => {
      unsubcribe()
    }
  }, [hotReload, id, repositories.ItemRepository])

  return { item }
}
