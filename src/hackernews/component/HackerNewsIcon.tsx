import { SvgIcon } from '@mui/joy'

export const HackerNewsIcon: React.FC = () => {
  return (
    <SvgIcon viewBox="4 4 188 188">
      <path d="m73.2521756 45.01 22.7478244 47.39130083 22.7478244-47.39130083h19.56569631l-34.32352071 64.48661468v41.49338532h-15.98v-41.49338532l-34.32352071-64.48661468z" />
    </SvgIcon>
  )
}
