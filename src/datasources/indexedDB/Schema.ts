import { DBSchema } from 'idb'
import { SharingModels } from '@/sharing'
import { HackerNewsModels } from '@/hackernews'

export type StoreCredential = { id: 'current'; codes: SharingModels.Code[] }

export type StorerItemId = { id: HackerNewsModels.ItemId }

export interface Schema extends DBSchema {
  credential: {
    key: string
    value: StoreCredential
  }

  skippedStories: {
    key: HackerNewsModels.ItemId
    value: StorerItemId
  }
}
