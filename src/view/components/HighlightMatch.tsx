import { Typography } from '@mui/joy'

type HightlightMatchProps = { value: string; sub: string }

export const HighlightMatch: React.FC<HightlightMatchProps> = ({ value, sub }: HightlightMatchProps) => {
  if (!sub) return value

  const chunks = new Array<{ valueChunk: string; important: boolean }>()

  for (let i = 0; i < value.length; i++) {
    if (value.substring(i, i + sub.length).toLocaleLowerCase() === sub.toLocaleLowerCase()) {
      chunks.push({ valueChunk: value.substring(i, i + sub.length), important: true })
      i += sub.length - 1
    } else {
      chunks.push({ valueChunk: value.substring(i, i + 1), important: false })
    }
  }

  return (
    <>
      {chunks.map((chunk, idx) =>
        chunk.important ? (
          <Typography color="success" fontWeight="900" key={idx}>
            {chunk.valueChunk}
          </Typography>
        ) : (
          chunk.valueChunk
        )
      )}
    </>
  )
}
