import { Check, Close } from '@mui/icons-material'
import { ButtonGroup, IconButton } from '@mui/joy'
import { useState, Children, cloneElement, ReactElement } from 'react'

export const InlineConfirmation: React.FC<{
  children: ReactElement
  confirmMessage?: string
  cancelMessage?: string
}> = ({ children, confirmMessage, cancelMessage }) => {
  const [clicked, setClicked] = useState(false)

  return clicked ? (
    <ButtonGroup spacing={1}>
      <IconButton title={confirmMessage || 'confirm'} onClick={() => children.props.onClick()} autoFocus>
        <Check />
      </IconButton>
      <IconButton title={cancelMessage || 'cancel'} onClick={() => setClicked(false)}>
        <Close />
      </IconButton>
    </ButtonGroup>
  ) : (
    cloneElement(Children.only(children), { onClick: () => setClicked(true) })
  )
}
