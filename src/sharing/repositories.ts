import { Credential, ConnectionResult, UserData } from './models'

type Unsubscribe = () => void

export interface SharingUserRepository {
  setCurrentCredential(credential: Credential): Promise<void>

  removeCurrentCredential(): Promise<void>

  getCurrentCredential(): Promise<Credential | null>

  onCurrentCredentialChanges(onchange: (credential: Credential | null) => Promise<void>): Unsubscribe
}

export interface SharingDataRepository {
  connect(credential: Credential): Promise<ConnectionResult>

  getUserData(): Promise<UserData>

  setUserData(data: UserData): Promise<void>

  onUserDataChanges(onchange: (data: UserData) => Promise<void>): Unsubscribe

  addSkippedStory(story: string): Promise<void>

  removeSkippedStory(story: string): Promise<void>
}

export interface Repositories {
  SharingUserRepository: SharingUserRepository
  SharingDataRepository: SharingDataRepository
}
