import { HackerNewsModels } from '@/hackernews'

export type UserData = {
  skippedStories: HackerNewsModels.ItemId[]
}
