import { Sharing } from './Activation'

export { Sharing }

export interface UseCases {
  readonly Sharing: Sharing
}
